package com.accenture.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan(basePackages = "com.accenture")
@PropertySource("classpath:com/accenture/properties/test.properties")
@Profile(value = "test")
public class TestConfig {

	@Value("${db.userName}")
	private String username;
	@Value("${db.password}")
	private String password;

	@Bean
	public static PropertySourcesPlaceholderConfigurer placeHolderConfigure() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean("details")
	public String getPropertiesDetails() {
		System.out.println("UserName : " + username);
		System.out.println("Password : " + password);
		
		return "dummy";
	}
}
