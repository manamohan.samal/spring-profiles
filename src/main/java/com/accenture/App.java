package com.accenture;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.accenture.config.DevConfig;
import com.accenture.config.ProdConfig;
import com.accenture.config.TestConfig;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	//for dev env
    	System.setProperty("spring.profiles.active", "dev");
    	ApplicationContext context = new AnnotationConfigApplicationContext(DevConfig.class);
    	String data = (String) context.getBean("details");
    	
    	
    	
    	
    	//for test env
//    	System.setProperty("spring.profiles.active", "test");
//    	ApplicationContext context2 = new AnnotationConfigApplicationContext(TestConfig.class);
//    	String data2 = (String) context2.getBean("details");
//    	
//    	
//    	
//    	
//    	
//    	//for prod env
//    	System.setProperty("spring.profiles.active", "prod");
//    	ApplicationContext context3 = new AnnotationConfigApplicationContext(ProdConfig.class);
//    	String data3 = (String) context3.getBean("details");
    	
    }
}
